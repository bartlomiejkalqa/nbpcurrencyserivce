package com.pretius.service.service.nbp;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class SameCurrenciesGivenException extends RuntimeException {

    private static final long serialVersionUID = 1236996191743114729L;

    SameCurrenciesGivenException() {
        super();
    }
}
