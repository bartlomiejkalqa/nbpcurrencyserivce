package com.pretius.service.service.config;

import com.pretius.service.service.CurrencyService;
import com.pretius.service.service.client.NbpServiceClient;
import com.pretius.service.service.nbp.CurrencyCalculator;
import com.pretius.service.service.nbp.NbpCurrencyService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CurrencyServiceConfig {

    @Bean
    protected CurrencyService nbpCurrencyService() {
        return new NbpCurrencyService(new NbpServiceClient(), new CurrencyCalculator());
    }
}
