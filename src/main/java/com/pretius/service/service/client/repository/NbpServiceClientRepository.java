package com.pretius.service.service.client.repository;

import com.pretius.service.service.client.model.Rates;

public class NbpServiceClientRepository {

    Rates collect;

    public NbpServiceClientRepository(Rates collect) {
        this.collect = collect;
    }

    public Rates getAllRates() {
        return collect;
    }
}
