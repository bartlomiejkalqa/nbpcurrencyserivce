package com.pretius.service.service.client;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import com.pretius.service.service.client.model.Rates;
import com.pretius.service.service.client.repository.NbpServiceClientRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

public class NbpServiceClient {

    private static final String BASE_URL = "http://api.nbp.pl/api/exchangerates/tables/A/";

    private final WebClient webClient;
    private NbpServiceClientRepository nbpServiceClientRepository;

    public NbpServiceClient() {
        this.webClient = WebClient.builder()
                .baseUrl(BASE_URL)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    public Rates getAllRates() {
        if (nbpServiceClientRepository != null) {
            return nbpServiceClientRepository.getAllRates();
        }
        final Rates rates = getRates();
        saveToCacheRepository(rates);
        return rates;
    }

    private Rates getRates() {
        return getAllExchangeRates()
                .orElseThrow(() -> new IllegalStateException("Not found"));
    }

    private Optional<Rates> getAllExchangeRates() {
        final Rates[] exchangeRatesResponse = getRateResponse();
        return getFirstRate(exchangeRatesResponse);
    }

    private Rates[] getRateResponse() {
        return webClient.get()
                .retrieve()
                .bodyToMono(Rates[].class)
                .block();
    }

    private Optional<Rates> getFirstRate(Rates[] exchangeRatesResponse) {
        return Arrays.stream(exchangeRatesResponse)
                .collect(Collectors.toList())
                .stream()
                .findFirst();
    }

    private void saveToCacheRepository(Rates rates) {
        this.nbpServiceClientRepository = new NbpServiceClientRepository(rates);
    }
}
