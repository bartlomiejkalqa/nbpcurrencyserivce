package com.pretius.service.service;

import java.math.BigDecimal;

import com.pretius.service.model.Currencies;

public interface CurrencyService {

    BigDecimal getCalculatedValue(BigDecimal amount, Currencies fromCurrency, Currencies toCurrency);
}
