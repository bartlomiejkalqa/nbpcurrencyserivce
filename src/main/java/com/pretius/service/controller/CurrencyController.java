package com.pretius.service.controller;

import java.math.BigDecimal;

import com.pretius.service.model.Currencies;
import com.pretius.service.service.CurrencyService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/currency")
public class CurrencyController {

    final CurrencyService currencyService;

    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    BigDecimal currency(@RequestParam BigDecimal amount,
                        @RequestParam Currencies fromCurrency,
                        @RequestParam Currencies toCurrency) {
        return currencyService.getCalculatedValue(amount, fromCurrency, toCurrency);
    }
}
