package com.pretius.service.service.nbp;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.pretius.service.model.Currencies;
import com.pretius.service.service.client.NbpServiceClient;
import com.pretius.service.service.client.model.Rate;
import com.pretius.service.service.client.model.Rates;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.when;

class NbpRatesServiceTest {

    private final NbpServiceClient client = Mockito.mock(NbpServiceClient.class);
    private final CurrencyCalculator calculator = new CurrencyCalculator();
    NbpCurrencyService nbpCurrencyService = new NbpCurrencyService(client, calculator);

    @Test
    void fromEURtoPLN() {
        // given
        final Currencies from = Currencies.EUR;
        final Currencies to = Currencies.PLN;
        final BigDecimal givenAmount = BigDecimal.valueOf(1.0);
        final BigDecimal expectedAmount = BigDecimal.valueOf(4.4832);
        when(client.getAllRates()).thenReturn(getMockedRates());
        // when
        final BigDecimal calculatedValue = nbpCurrencyService.getCalculatedValue(givenAmount, from, to);
        // then
        assertThat(expectedAmount).isEqualTo(calculatedValue);
    }

    @Test
    void fromPLNtoEUR() {
        // given
        final Currencies from = Currencies.PLN;
        final Currencies to = Currencies.EUR;
        final BigDecimal givenAmount = BigDecimal.valueOf(1.0);
        final BigDecimal expectedAmount = BigDecimal.valueOf(0.2231);
        when(client.getAllRates()).thenReturn(getMockedRates());
        // when
        final BigDecimal calculatedValue = nbpCurrencyService.getCalculatedValue(givenAmount, from, to);
        // then
        assertThat(expectedAmount).isEqualTo(calculatedValue);
    }

    @Test
    void fromEURtoEUR() {
        // given
        final Currencies from = Currencies.EUR;
        final Currencies to = Currencies.EUR;
        final BigDecimal givenAmount = BigDecimal.valueOf(1.0);
        when(client.getAllRates()).thenReturn(getMockedRates());
        // when
        assertThatThrownBy(() -> { nbpCurrencyService.getCalculatedValue(givenAmount, from, to);})
                .isInstanceOf(SameCurrenciesGivenException.class);
    }

    @Test
    void fromCHFtoEUR() {
        // given
        final Currencies from = Currencies.CHF;
        final Currencies to = Currencies.EUR;
        final BigDecimal givenAmount = BigDecimal.valueOf(1.0);
        final BigDecimal expectedAmount = BigDecimal.valueOf(0.9269);
        when(client.getAllRates()).thenReturn(getMockedRates());
        // when
        final BigDecimal calculatedValue = nbpCurrencyService.getCalculatedValue(givenAmount, from, to);
        // then
        assertThat(expectedAmount).isEqualTo(calculatedValue);
    }

    @Test
    void fromCHFtoEURWhenAmountEqual10() {
        // given
        final Currencies from = Currencies.CHF;
        final Currencies to = Currencies.EUR;
        final BigDecimal givenAmount = BigDecimal.valueOf(10.0);
        final BigDecimal expectedAmount = BigDecimal.valueOf(9.2690).setScale(4, RoundingMode.HALF_UP);
        when(client.getAllRates()).thenReturn(getMockedRates());
        // when
        final BigDecimal calculatedValue = nbpCurrencyService.getCalculatedValue(givenAmount, from, to);
        // then
        assertThat(expectedAmount).isEqualTo(calculatedValue);
    }

    private Rates getMockedRates() {
        final List<Rate> arrayList = new ArrayList<>();
        arrayList.add(new Rate("4.4832", "EUR"));
        arrayList.add(new Rate("4.1555", "CHF"));
        return new Rates(arrayList);
    }
}